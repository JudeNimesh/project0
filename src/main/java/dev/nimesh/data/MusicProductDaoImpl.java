package dev.nimesh.data;

import dev.nimesh.models.MusicProduct;
import dev.nimesh.util.ConnectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.plaf.nimbus.State;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MusicProductDaoImpl implements  MusicProductDao {

    private Logger logger = LoggerFactory.getLogger(MusicProductDaoImpl.class);

    //@Override
    public List<MusicProduct> getAllProducts() {
        List<MusicProduct> items = new ArrayList<>();

        // try with resources - Connection is able to be used here because it implements Autocloseable
        try (Connection connection = ConnectionUtil.getConnection()){
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from product_items");

            // result set pointer is before the first record

            while(resultSet.next()){ // moves the pointer to the next record, returning false if there are no more record
                //process each record in the result set
                int prod_id = resultSet.getInt("id");
                String prod_name  = resultSet.getString("prod_name");
                String deprt_name = resultSet.getString("deprt_name");
                double price = resultSet.getDouble("price");
                int stock = resultSet.getInt("stock");
                MusicProduct item = new MusicProduct(prod_id, prod_name, deprt_name, price, stock);
                items.add(item);
                logger.info("TEST: 300 prod_id "+ prod_id+", prod_name "+prod_name+", "+deprt_name+", "+price+", "+stock);
            }
            logger.info("selecting all items from db - "+ items.size()+ " item(s) retrieved");

        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }
        return items;
    }

    @Override
    public MusicProduct getProductById(int prod_id) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement prepareStatement = connection.prepareStatement("select * from product_items where id = ?");
            prepareStatement.setInt(1,prod_id);
            ResultSet resultSet = prepareStatement.executeQuery();
            if(resultSet.next()){
                String prod_name  = resultSet.getString("prod_name");
                double price = resultSet.getDouble("price");
                int stock = resultSet.getInt("stock");
                String deprt_name = resultSet.getString("deprt_name");
                logger.info("1 item retrieved from database by prod_id: " + prod_id);
                logger.info("TEST: 310 prod_id "+ prod_id+", prod_name: "+prod_name+", "+deprt_name+", "+price+", "+stock);

                logger.info("320: Product Item:"+ new MusicProduct(prod_id, prod_name, deprt_name, price, stock));
                return new MusicProduct(prod_id, prod_name, deprt_name, price, stock);
            }
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return null;
    }

    @Override
    public MusicProduct addNewProduct(MusicProduct item) {
        try(Connection connection = ConnectionUtil.getConnection()){
            PreparedStatement preparedStatement =
             connection.prepareStatement("insert into product_items (prod_name, price, stock, deprt_name) values (?, ?, ?, ?)");
            preparedStatement.setString(1, item.getProdName());
            preparedStatement.setDouble(2, item.getPrice());
            preparedStatement.setInt(3, item.getStock());
            preparedStatement.setString(4, item.getDepartmentName());

            preparedStatement.executeUpdate();
            logger.info("successfully added new item to the db");
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return item;
    }

    @Override
    public void deleteProduct(int prod_id) {
        try (Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("delete from product_items where id = ?"); //can use name, price
           logger.info("Removing the product item with a ID: "+ prod_id);
            preparedStatement.setInt(1, prod_id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                double price = resultSet.getDouble("price");
                String prod_name = resultSet.getString("prod_name");
                String deprt_name = resultSet.getString("deprt_name");
                int stock = resultSet.getInt("stock");
                logger.info("1 item deleted from database by id: " + prod_id);
                new MusicProduct(prod_id,prod_name,deprt_name,price,stock);
            }
        } catch (SQLException e) {
            logger.error(e.getClass() + "  " + e.getMessage());
        }
    }

    @Override
    public MusicProduct updateProduct(MusicProduct oldItem, MusicProduct newItem) {
        try (Connection connection = ConnectionUtil.getConnection()) {

            int prod_id = oldItem.getProdId();
            String prod_name = newItem.getProdName();
            String deprt_name = newItem.getDepartmentName();
            double price = newItem.getPrice();
            int stock = newItem.getStock();

            PreparedStatement preparedStatement = connection.prepareStatement("select * from product_items where id = ?"); //can use name, price
            preparedStatement.setInt(1, prod_id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {

                String oldName  = resultSet.getString("prod_name");
                String oldDep_name = resultSet.getString("deprt_name");
                double oldPrice = resultSet.getDouble("price");
                int oldStock = resultSet.getInt("stock");
                logger.info("1 item retrieved from database by prod_id: " + prod_id + ", prod_name: " + oldName+
                        ", deprt_name: " + oldDep_name +", price: " + oldPrice + ", stock: " + oldStock);

                PreparedStatement preparedStatement2 =
                 connection.prepareStatement("update product_items " +
                         "set prod_name = ?, price = ?, stock = ?, deprt_name = ?, where id = ?"); //can use name, price

                preparedStatement2.setString(1, prod_name);
                preparedStatement2.setDouble(2, price);
                preparedStatement2.setInt(3, stock);
                preparedStatement2.setString(4, deprt_name);
                preparedStatement2.setInt(5, prod_id);

                preparedStatement2.executeUpdate();
                logger.info("successfully updated a new item to the db");
                logger.info("1 item updated to the database by prod_id: " +prod_id+ ", prod_name: "+prod_name+
                        ", price: "+price + ", stock: "+stock+", deprt_name: "+deprt_name);
                return new MusicProduct(prod_id, prod_name, deprt_name, price, stock);
            }
        } catch (SQLException e) {
            logger.error(e.getClass() + "  " + e.getMessage());
        }
        return null;
    }

    @Override
    public List<MusicProduct> getItemsInRange(double max, double min) {
        List<MusicProduct> items = new ArrayList<>();

        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement prepareStatement = connection.prepareStatement("select * from product_items where price > ? and price < ?");
            prepareStatement.setDouble(1,min);
            prepareStatement.setDouble(2,max);
            ResultSet resultSet = prepareStatement.executeQuery();
            if(resultSet.next()){
                int prod_id = resultSet.getInt("prod_id");
                String prod_name = resultSet.getString("prod_name");
                String deprt_name = resultSet.getString("deprt_name");
                double price = resultSet.getDouble("price");
                int stock = resultSet.getInt("stock");
                items.add(new MusicProduct(prod_id, prod_name, deprt_name, price, stock));
            }
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return items;
    }

    @Override
    public List<MusicProduct> getItemWithMaxPrice(double max) {
        List<MusicProduct> items = new ArrayList<>();

        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement prepareStatement = connection.prepareStatement("select * from product_items where price < ?");
            prepareStatement.setDouble(1,max);
            ResultSet resultSet = prepareStatement.executeQuery();
            if(resultSet.next()){
                int prod_id = resultSet.getInt("prod_id");
                String prod_name = resultSet.getString("prod_name");
                String deprt_name = resultSet.getString("deprt_name");
                double price = resultSet.getDouble("price");
                int stock = resultSet.getInt("stock");
                items.add(new MusicProduct(prod_id, prod_name, deprt_name, price, stock));
            }
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return items;
    }

    @Override
    public List<MusicProduct> getItemWithMinPrice(double min) {
        List<MusicProduct> items = new ArrayList<>();

        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement prepareStatement = connection.prepareStatement("select * from product_items where price > ?");
            prepareStatement.setDouble(1,min);
            ResultSet resultSet = prepareStatement.executeQuery();
            if(resultSet.next()){
                int prod_id = resultSet.getInt("prod_id");
                String prod_name = resultSet.getString("prod_name");
                String deprt_name = resultSet.getString("deprt_name");
                double price = resultSet.getDouble("price");
                int stock = resultSet.getInt("stock");
                items.add(new MusicProduct(prod_id, prod_name, deprt_name, price, stock));
            }
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return items;
    }

}
