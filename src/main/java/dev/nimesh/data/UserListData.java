package dev.nimesh.data;

import dev.nimesh.models.UserList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/*
    this was our original data access method with collections handling in memory items
 */

public class UserListData implements UserListDao {

    private List<UserList> users = new ArrayList<>();
    Logger logger = LoggerFactory.getLogger(CustomerListData.class);

    public UserListData(){
        super();
    }
    public List<UserList> getAllUsers(){
        return new ArrayList<>(users);
    }

    public UserList getUserById(String username){

        return users.stream().filter(user->user.getUsername()==username).findAny().orElse(null);
    }

    public UserList addNewUser(UserList user){
        users.add(user);
        return user;
    }

    public void deleteUser(String username){
//      items.removeIf(customerList -> (customerList!=null)?id==customerList.getId():false);
        Predicate<UserList> idCheck = customerList -> customerList!=null && username==customerList.getUsername();
        users.removeIf(idCheck);
        // this remove operation can also be handled using iteration, looping through and using the
        // list remove method
    }
    public UserList updateNewUser(UserList user, UserList newUser) {

        UserList oldUser = users.stream().filter(customerList -> customerList.getUsername() == user.getUsername()).findAny().orElse(null);
        logger.info("Before Updating to an old item: " + oldUser);
        logger.info("The new item replace on the indexOf(oldUser)=" + users.indexOf(oldUser) + "<-- new user: " + newUser);
        users.set(users.indexOf(oldUser), newUser);

        logger.info("After Updating to a new user: " + newUser);

        return newUser;  //
    }
}



