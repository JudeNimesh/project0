package dev.nimesh;

import dev.nimesh.controllers.*;
import io.javalin.Javalin;

import static io.javalin.apibuilder.ApiBuilder.*;
import static io.javalin.apibuilder.ApiBuilder.post;

public class MusicJavalinApp {

    ProductController productController = new ProductController();
    OrderListController orderListController = new OrderListController();
    CustomerController customerController = new CustomerController();
    AuthController authController = new AuthController();
    UserController userController = new UserController();

    //app.get("/", (Context ctx)-> {ctx.result("Hello Welcome to my Music store");});
    //app.get("/", ctx -> ctx.result("Please follow the direction to access Music products:"));
    Javalin app = Javalin.create().routes( ()->{

        path("/items", ()-> {
            before("/", authController::authorizeToken);
            get(productController::handleGetProductRequest);
            post(productController::handlePostNewProduct);

            path("/:id", () -> {
                before("/", authController::authorizeToken);
                get(productController::handleGetProductByIdRequest);
                delete(productController::handleDeleteProductById);
                put(productController::handleUpdateProductById);
            });
        });

        //before("/users", authController::authorizeToken);
        path("/users", ()-> {
            before("/", authController::authorizeToken);
            get(userController::handleGetUserRequest);
            post(userController::handlePostNewUser);
            path("/:username", () -> {
                before("/", authController::authorizeToken);
                get(userController::handleGetUserByIdRequest);
                delete(userController::handleDeleteUserById);
                put(userController::handleUpdateUserById);
            });
        });

        path("/customers", ()-> {
            before("/", authController::authorizeToken);
            get(customerController::handleGetCustomerRequest);
            post(customerController::handlePostNewCustomer);
            path("/:id", () -> {
                before("/", authController::authorizeToken);
                get(customerController::handleGetCustomerByIdRequest);
                delete(customerController::handleDeleteCustomerById);
                put(customerController::handleUpdateCustomerById);
            });
        });

        path("/orders", ()->{
            before("/", authController::authorizeToken);
            get(orderListController::handleGetOrderRequest);
            post(orderListController::handlePostNewOrder);
            path("/:id",()->{
                before("/", authController::authorizeToken);
                get(orderListController::handleGetOrderByIdRequest);
                delete(orderListController::handleDeleteOrderById);
                put(orderListController::handleUpdateOrderById);
            });
        });

        path("/login",()->{
            post(authController::authenticateLogin);
        });
    });

    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}
