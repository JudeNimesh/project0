package dev.nimesh.models;

import java.io.Serializable;
import java.util.Objects;

public class UserList  implements Serializable {

    private String username;
    private String password;

    public UserList(){
        super();
    }

    public UserList(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UserList(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserList userList = (UserList) o;
        return Objects.equals(username, userList.username) && Objects.equals(password, userList.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password);
    }

    @Override
    public String toString() {
        return "UserList{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}