package dev.nimesh.models;

import java.io.Serializable;
import java.util.Objects;

import dev.nimesh.data.MusicProductDao;
import dev.nimesh.models.MusicProduct;
import dev.nimesh.models.CustomerList;


public class OrderList implements Serializable {

    private int order_id;
    private String order_name;
    private int cust_id;
    private int prod_id;
    private int qty;
    private double total;


//---------from class MusicProduct ------
    private MusicProduct musicProduct;
    private String prod_name;
    private double price;
    //---------from class CustomerList ------
    private CustomerList customer;
    private String cust_name;

//---------------------------------------

    public OrderList(){
        super();
    }

    public OrderList(int order_id, String order_name, int cust_id, int prod_id, int qty, double total) {
        super();
        this.order_id   = order_id;
        this.order_name = order_name;
        this.qty       = qty;
        this.total     = total;
        this.customer = customer;
        this.musicProduct = musicProduct;
        this.prod_id = prod_id;
    }

    public OrderList(int order_id, String order_name, int cust_id, int prod_id, int qty, double total,
                     CustomerList customer, MusicProduct musicProduct) {
        super();
        this.order_id   = order_id;
        this.order_name = order_name;
        this.qty       = qty;
        this.total     = total;
        this.customer = customer;
        this.musicProduct = musicProduct;
        this.prod_id = prod_id;
    }

    public OrderList(int order_id) {
        this.order_id = order_id;
    }
    public OrderList(String order_name) {
        this.order_name = order_name;
    }

    public OrderList(MusicProduct musicProduct) {
        this.prod_name = musicProduct.getProdName();
        this.price = musicProduct.getPrice();

    }

    public OrderList(CustomerList customer) {
        this.cust_name = customer.getCustomerName();
    }

    public int getOrderId() { return order_id; }

    public void setOrderId(int order_id) { this.order_id = order_id; }

    public String getOrderName() {
        return order_name;
    }

    public void setOrderName(String order_name) {
        this.order_name = order_name;
    }

    public double getOrderTotal() { return total; }

    public void setOrderTotal(double total) { this.total = total; }

    public MusicProduct getMusicProduct() {
        return musicProduct;
    }

    public void setMusicProduct(MusicProduct musicProduct) {
        this.musicProduct = musicProduct;
    }

    public CustomerList getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerList customer) {
        this.customer = customer;
    }

    public int getProductId() { return order_id; }

    public void setProductId(int prod_id) { this.prod_id = prod_id; }

    public int getOrderQuantity() { return qty; }

    public void setOrderQuantity(int qty) { this.qty = qty; }

}

