package dev.nimesh.controllers;

import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthController {

    private Logger logger = LoggerFactory.getLogger(AuthController.class);

    public void authenticateLogin(Context ctx){
        // getting params from what would be a form submission (Content-Type: application/x-www-form-urlencoded)
        String user = ctx.formParam("username");
        String pass = ctx.formParam("password");
        logger.info(user +" attempted login");
        if(user!=null && user.equals("jnimesh")){
            if(pass!=null && pass.equals("password")){
                logger.info("successful login");
                // send back token
                ctx.header("Authorization", "admin-auth-token");
                ctx.status(200);
                return;
            }
        }
        throw new UnauthorizedResponse("Credentials were incorrect");
    }

    public void authorizeToken(Context ctx){
        logger.info("attempting to authorize token");

        // getting the "Authorization" header from the incoming request
        String authHeader = ctx.header("Authorization");

        if(authHeader!=null && authHeader.equals("admin-auth-token")){
            logger.info("request is authorized, proceeding to handler method "+ authHeader);
        } else {
            logger.warn("improper authorization "+authHeader);
            throw new UnauthorizedResponse();
        }
    }


}

