package dev.nimesh.services;

import dev.nimesh.data.UserListDao;
import dev.nimesh.data.UserListDaoImpl;
import dev.nimesh.models.UserList;

import java.util.List;

public class UserService {

    private UserListDao userListDao = new UserListDaoImpl();

    public List<UserList> getAll() {
            return userListDao.getAllUsers();
    }

    public UserList getUserListById(String username) {
        System.out.println("500: User username: "+username+"\n"+ userListDao.getUserById(username).toString() );
        return userListDao.getUserById(username);
    }

    public UserList add(UserList user) {
        return userListDao.addNewUser(user);
    }

    public void delete(String username) {
        userListDao.deleteUser(username);
    }

    public void update(UserList oldUser, UserList newUser) {
        userListDao.updateNewUser(oldUser, newUser);
    }

}

