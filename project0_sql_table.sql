-------------------------------------------------------------
-- Clean up the tables by removing
-------------------------------------------------------------
drop table user_list;

drop table customer_list;

drop table order_list;

drop table product_items;

drop table user_list;
-------------------------------------------------------------
--Create tables
------------------------------------------------------------
create table customer_list(
  	id serial primary key,
	cust_name varchar(50)  not null,
  	address varchar(80) not null,
  	email varchar(40) unique not null,
  	phone varchar(20) not null,
  	username varchar(20) references user_list ( username)
);

--------------------------------------------------------------------------

create table user_list(
  	username varchar(20) unique,
  	password varchar(20) not null
);
--------------------------------------------------------------------------  
create table product_items(
	  id serial primary key,
	  prod_name varchar(50) not null,
	  price numeric (6,2) not null,
	  stock numeric not null,
	  deprt_name varchar(50)
);
	    
--------------------------------------------------------------------------	  
create table order_list(
	  order_id serial primary key,
	  order_name varchar(50),
	  qty   int,
	  total numeric (6,2),
 	  cust_id int references customer_list ( id),
	  prod_id int references product_items ( id)
);

---------------------------------------------------------------------------

insert into user_list ( username, password ) values ('jdoe2', '112233');

insert into user_list ( username, password ) values ('rmanuel', '123456');

insert into user_list ( username, password ) values ('jdoe1', '112233');		 
		
insert into user_list ( username, password ) values ('jselba', 'pasw0rd');	

insert into user_list ( username, password ) values ('mkirsch', 'TeaPot18');		 
		
insert into user_list ( username, password ) values ('jbenadic', 'keyPsa*');		

--------------------------------------------------------------------------------

insert into customer_list (cust_name, address, email, phone, username ) 
values ('John Doe', '25 Main St. Reston, VA 20190', 'john.dove@gmail.com', '703-555-1212','jdoe2');

insert into customer_list (cust_name, address, email, phone, username ) 
values ('Rajesh Manuel', '1098 Fairfax Rd. Chantily, VA 20163', 'rajesh.manuel@gmail.com', '703-404-1123', 'rmanuel');
insert into customer_list (cust_name, address, email, phone, username ) 
values ('Prakash Joseph', '1587 ashburn Ave. South Reading, VA 20178', 'pjoseph@gmail.com', '703-555-1456', 'jdoe1' );		 
		
insert into customer_list (cust_name, address, email, phone, username ) 
values ('Jeba Seva', '4589 ashburn Ave. Herndon, VA 20172', 'Jeba.selva@gmail.com', '703-555-5478', 'jselba');	

insert into customer_list (cust_name, address, email, phone, username ) 
values ('Mike Kirsch', '44330 Woodridge Pkwy. Lessburg, VA 20176', 'mkirsch@gmail.com', '301-478-6748', 'mkirsch');		 
		
insert into customer_list (cust_name, address, email, phone, username  ) 
values ('John Benedic', '1245 Lowes Ave. Fiarfax, VA 20163', 'jbenadic@gmail.com','404-258-1212', 'jbenadic');		
------------------------------------------------------------------------------------------------	

insert into product_items (prod_name , price, stock, deprt_name ) 
values ('Guitar', 650.99, 30, 'Instruments');	 

insert into product_items (prod_name , price, stock, deprt_name ) 
values ('Electric Guitar', 1250.99, 10, 'Instruments');	

insert into product_items (prod_name , price, stock, deprt_name ) 
values ('Drums', 2500, 6, 'Instruments');

insert into product_items (prod_name , price, stock, deprt_name ) 
values ('Electric Drums', 2500, 3, 'Instruments');

insert into product_items (prod_name , price, stock, deprt_name ) 
values ('Analog Mixer', 610.89, 15, 'Audio');

insert into product_items (prod_name , price, stock, deprt_name ) 
values ('Speakers', 320.95, 10, 'Audio');

insert into product_items (prod_name , price, stock, deprt_name ) 
values ('Spot Beam', 550.95, 5, 'Lighting');

insert into product_items (prod_name , price, stock, deprt_name ) 
values ('Digital Camera', 1100.95, 5, 'Video');

insert into product_items (prod_name , price, stock, deprt_name ) 
values ('Spot Beam', 550.95, 5, 'Lighting');

insert into product_items (prod_name , price, stock, deprt_name ) 
values ('Video Controler', 600.95, 5, 'Video');
------------------------------------------------------------------------------

insert into order_list ( order_name, cust_id, prod_id,  qty, total ) 
values ('Gift', 1,  5,  10, 0 );		

insert into order_list ( order_name, cust_id, prod_id,  qty, total ) 
values ('Custom Order', 1,  6,  5,  0);	
		
insert into order_list (order_name, cust_id, prod_id, qty, total) 
values ('Special Order', 4, 8, 10, 0);	

insert into order_list (order_name, cust_id, prod_id, qty, total) 
values ('Charity Order', 3, 3, 75, 0);	

insert into order_list (order_name, cust_id, prod_id, qty, total) 
values ('VTA Order', 4, 2, 10, 0);	

insert into order_list (order_name, cust_id, prod_id, qty, total) 
values ('Custom Order', 6, 7, 10, 0);	

select cust_name, email, address, prod_name, price, qty 
from customer_list, order_list, product_items 
where  customer_list.cust_id = order_list.cust_id 
and order_list.prod_id = product_items.prod_id;

select * from product_items where prod_id = ?

select * from product_items;
select * from product_items where prod_id = 1

select * from order_list;
select * from order_list where order_id = 12;

select * from customer_list;
select * from customer_list where cust_id = 3;

------------------------------------end of table creation ---------------------------------

select order_id, order_name, cust_id, cust_name, prod_id, prod_name, price, qty, total, deprt_name, address, email, phone
from customer_list, order_list, product_items 
where  customer_list.id = order_list.cust_id 
and order_list.prod_id = product_items.id;

select order_id, order_name,  cust_name, prod_id, prod_name, price, qty, total, deprt_name, address, email, phone
from customer_list, order_list, product_items 
where  (customer_list.id = order_list.cust_id 
and order_list.prod_id = product_items.id) and order_id = 6;

select order_id, order_name, cust_id, cust_name, email, address, prod_name, price, qty 
from customer_list, order_list, product_items 
where  customer_list.id = order_list.cust_id 
and order_list.prod_id = product_items.id;

select order_id, order_name, cust_id, cust_name, qty from order_list, customer_list where  customer_list.id = order_list.cust_id;

delete from user_list where username = 8;
select order_id, order_name, cust_id, 
                    cust_name, prod_id, prod_name, price, qty, stock total, deprt_name, address, email, phone 
                    from customer_list, order_list, product_items where  customer_list.id = order_list.cust_id 
                     and order_list.prod_id = product_items.id and order_id = 5;
                     
insert into customer_list (cust_id, cust_name, address, email, phone, username) " +
                     "values (7, 'Jude Winslow', '1328 Dolley St. McLean, VA 20190', 'jnimesh@gmail.com', '703-658-1478', 'jnimesh');    
7, cust_name='Jude Winslow', address='1328 Dolley St. McLean, VA 20190', email='jnimesh@gmail.com', phone='703-658-1478', username='jnimesh
'}                     





delete from user_list where username = 8;
delete from customer_list where id = 9;
